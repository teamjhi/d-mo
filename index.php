<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
		<style>
		.banner{
			clear: both; 
			width: 100%; 
			font-size: 14px; 
			font-family: Helvetica; 
			color: #30121d; 
			background: #bcbf77; 
			padding: 20px; 
			text-align: center;				
		}
		</style>
    </head>
    <body>
        <?php
        /***
         * Ceci est une modification
         */


		/**
		 * @filename: currentgitbranch.php
		 * @usage: Include this file after the '<body>' tag in your project
		 * @author Kevin Ridgway 
		 */

		function git_version(){
			$stringfromfile = file('.git/HEAD', FILE_USE_INCLUDE_PATH);
			$firstLine = $stringfromfile[0]; //get the string from the array
			$explodedstring = explode("/", $firstLine, 3); //seperate out by the "/" in the string
			$branchname = rtrim($explodedstring[2]); //get the one that is always the branch name
			$handle = fopen(getcwd()."/.git/logs/refs/heads/".$branchname."", "r");

			if ($handle) {
				while (($buffer = fgets($handle, 4096)) !== false) {
					echo $buffer;
					if(strpos($buffer,"commit")){
						$version = substr($buffer,strpos($buffer,"commit")+7);
						$version = substr($version,0,strpos($version,"-"));
					}
				}
			/*	if (!feof($handle)) {
					echo "Erreur: fgets() a échoué\n";
				}*/
				fclose($handle);
			}
			return " <span title= \"".$branchname."\"> ".$version."</span>";

		 }			
			echo "<div class=\"banner\">Current branch version: <span style='color:#fff; font-weight: bold; text-transform: uppercase;'>" .git_version() ."</span></div>"; //show it on the page


        ?>
		<p><b>Astuces : </b><br> Vous pouvez copier les lignes ci-dessous et les coller dans votre console SSH avec le bouton droit de la souris</p>

		<h1>Configuration sur le serveur</h1>
		<h2>Clone</h2>
		<p>Se connecter en ssh et saisir la commande</p>
		<pre>
		git clone git@bitbucket.org:teamjhi/d-mo.git
		</pre>
		<h2>Commit</h2>
		<h3>Cas normal</h3>
		<pre>
		git commit -a -m "debug des branches generiques" --author=jacques_hirtzel
		Vous devez ensuite saisr le code de votre clef SSH
		</pre>
		<h2>Push</h2>
		<pre>
		git push
		Vous devez ensuite saisr le code de votre clef SSH
		</pre>
		<h3>Modification du dernier message de commit</h3>
		<pre>
		git commit --amend 
		pour quitter l'éditeur et enregistrer les modifications [esc] :wq (sans enregistrer [esc] :q) 
		</pre>
		<h2>Créer une branche</h2>
		<pre>
		git checkout -b nom_de_la_branche_a_creer
		commit
		</pre>
		<h2>Changer de branche</h2>
		<pre>
		git checkout nom_de_la_branche_choisie
		</pre>
		<h3>Modification du dernier message de commit</h3>
    </body>
</html>
bycvyyxcvxycvcyxvycyv